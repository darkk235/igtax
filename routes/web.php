<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Library\BooksController;
use App\Http\Controllers\Library\CategoryController;
use App\Http\Controllers\Library\VideosController;
use App\Http\Controllers\Taxbase\TaxBaseController;
use App\Http\Controllers\Taxbase\TaxCategoryController;
use App\Http\Controllers\Taxbase\TaxDetailController;
use App\Http\Controllers\Taxbase\TaxTypeController;
use App\Http\Controllers\Users\UsersController;
use App\Http\Controllers\User\DashboardController as DashboardUser;
use App\Http\Controllers\User\ProfileController as UserProfileController;
use App\Http\Controllers\User\LibraryController as UserLibrary;
use App\Http\Controllers\User\TaxBaseController as UserTaxBaseController;

Route::middleware(['auth', 'verified', 'roleCheck:admin'])->group(function () {
    Route::get('/adminDashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    Route::prefix('library')->group(function () {
        Route::prefix('category')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('library.category');
            Route::get('/add', [CategoryController::class, 'add'])->name('library.category.add');
            Route::post('/store', [CategoryController::class, 'store'])->name('library.category.store');
            Route::get('/edit/{ids}', [CategoryController::class, 'edit'])->name('library.category.edit');
            Route::put('/update/{ids}', [CategoryController::class, 'update'])->name('library.category.update');
            Route::delete('/destroy/{ids}', [CategoryController::class, 'destroy'])->name('library.category.delete');
        });

        Route::prefix('books')->group(function () {
            Route::get('/', [BooksController::class, 'index'])->name('library.books');
            Route::get('/add', [BooksController::class, 'add'])->name('library.books.add');
            Route::post('/store', [BooksController::class, 'store'])->name('library.books.store');
            Route::get('/edit/{ids}', [BooksController::class, 'edit'])->name('library.books.edit');
            Route::put('/update/{ids}', [BooksController::class, 'update'])->name('library.books.update');
            Route::delete('/delete/{ids}', [BooksController::class, 'destroy'])->name('library.books.delete');
        });

        Route::prefix('videos')->group(function () {
            Route::get('/', [VideosController::class, 'index'])->name('library.videos');
            Route::get('/add', [VideosController::class, 'add'])->name('library.videos.add');
            Route::post('/store', [VideosController::class, 'store'])->name('library.videos.store');
            Route::get('/edit/{ids}', [VideosController::class, 'edit'])->name('library.videos.edit');
            Route::put('/update/{ids}', [VideosController::class, 'update'])->name('library.videos.update');
            Route::delete('/delete/{ids}', [VideosController::class, 'destroy'])->name('library.videos.delete');
        });
    });

    Route::prefix('tax')->group(function () {
        Route::prefix('category')->group(function () {
            Route::get('/', [TaxCategoryController::class, 'index'])->name('tax.category');
            Route::get('/add', [TaxCategoryController::class, 'add'])->name('tax.category.add');
            Route::post('/store', [TaxCategoryController::class, 'store'])->name('tax.category.store');
            Route::get('/edit/{ids}', [TaxCategoryController::class, 'edit'])->name('tax.category.edit');
            Route::put('/update/{ids}', [TaxCategoryController::class, 'update'])->name('tax.category.update');
            Route::delete('/destroy/{ids}', [TaxCategoryController::class, 'destroy'])->name('tax.category.delete');
        });

        Route::prefix('base')->group(function () {
            Route::get('/', [TaxBaseController::class, 'index'])->name('tax.base');
            Route::get('/add', [TaxBaseController::class, 'add'])->name('tax.base.add');
            Route::post('/store', [TaxBaseController::class, 'store'])->name('tax.base.store');
            Route::get('/edit/{ids}', [TaxBaseController::class, 'edit'])->name('tax.base.edit');
            Route::put('/update/{ids}', [TaxBaseController::class, 'update'])->name('tax.base.update');
            Route::delete('/delete/{ids}', [TaxBaseController::class, 'destroy'])->name('tax.base.delete');
        });

        Route::prefix('detail')->group(function () {
            Route::get('/{ids}', [TaxDetailController::class, 'index'])->name('tax.detail');
            Route::get('/add/{ids}', [TaxDetailController::class, 'add'])->name('tax.detail.add');
            Route::post('/store/{ids}', [TaxDetailController::class, 'store'])->name('tax.detail.store');
            Route::get('/edit/{ids}', [TaxDetailController::class, 'edit'])->name('tax.detail.edit');
            Route::put('/update/{ids}', [TaxDetailController::class, 'update'])->name('tax.detail.update');
            Route::delete('/delete/{ids}', [TaxDetailController::class, 'destroy'])->name('tax.detail.delete');
        });

        Route::prefix('type')->group(function () {
            Route::get('/', [TaxTypeController::class, 'index'])->name('tax.type');
            Route::get('/add', [TaxTypeController::class, 'add'])->name('tax.type.add');
            Route::post('/store', [TaxTypeController::class, 'store'])->name('tax.type.store');
            Route::get('/edit/{ids}', [TaxTypeController::class, 'edit'])->name('tax.type.edit');
            Route::put('/update/{ids}', [TaxTypeController::class, 'update'])->name('tax.type.update');
            Route::delete('/delete/{ids}', [TaxTypeController::class, 'destroy'])->name('tax.type.delete');
        });
    });

    Route::prefix('users')->group(function () {
        Route::get('/', [UsersController::class, 'index'])->name('users');
        Route::get('/edit/{ids}', [UsersController::class, 'edit'])->name('users.edit');
        Route::put('/update/{ids}', [UsersController::class, 'update'])->name('users.update');
        Route::delete('/delete/{ids}', [UsersController::class, 'destroy'])->name('users.delete');
    });
});

Route::middleware(['auth', 'verified', 'roleCheck:users'])->group(function () {
    Route::get('/userDashboard', [DashboardUser::class, 'index'])->name('users.dashboard');

    Route::prefix('profile')->group(function () {
        Route::get('/edit', [UserProfileController::class, 'edit'])->name('profile.edit');
        Route::patch('/update', [UserProfileController::class, 'update'])->name('profile.update');
    });

    Route::prefix('library')->group(function () {
        Route::get('/reading', [UserLibrary::class, 'reading'])->name('library.reading');
        Route::get('/codex', [UserLibrary::class, 'codex'])->name('library.codex');
    });

    Route::prefix('taxbase')->group(function () {
        Route::get('/', [UserTaxBaseController::class, 'index'])->name('taxbase');
        Route::match(['get','post'],'/show/{id_tax_base}', [UserTaxBaseController::class, 'show'])->name('taxbase.show');
        Route::get('/reading/{id_tax_base_detail}', [UserTaxBaseController::class, 'reading'])->name('taxbase.reading');
        Route::match(['get','post'],'/countrys/{id_tax_base}', [UserTaxBaseController::class, 'countrys'])->name('taxbase.countrys');
        Route::get('/country/{id_tax_base_detail}', [UserTaxBaseController::class, 'country'])->name('taxbase.country');
    });
});

require __DIR__ . '/auth.php';
