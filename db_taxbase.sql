-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 21, 2024 at 06:41 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_taxbase`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) NOT NULL,
  `connection` text NOT NULL,
  `queue` text NOT NULL,
  `payload` longtext NOT NULL,
  `exception` longtext NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(23, '2014_10_12_000000_create_users_table', 1),
(24, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(25, '2019_08_19_000000_create_failed_jobs_table', 1),
(26, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(27, '2024_01_04_011727_tbl_library_category', 1),
(28, '2024_01_04_011935_tbl_library_book', 1),
(29, '2024_01_04_012639_tbl_library_video', 1),
(30, '2024_01_07_060721_tbl_tax_base', 1),
(31, '2024_01_07_060732_tbl_tax_base_detail', 1),
(32, '2024_01_07_060743_tbl_tax_category', 1),
(33, '2024_01_07_061153_tbl_tax_type', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_library_book`
--

CREATE TABLE `tbl_library_book` (
  `id_library_book` bigint(20) UNSIGNED NOT NULL,
  `category_code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `publication_year` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `isbn` varchar(255) NOT NULL,
  `file_upload` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_library_book`
--

INSERT INTO `tbl_library_book` (`id_library_book`, `category_code`, `title`, `cover`, `author`, `publication_year`, `publisher`, `isbn`, `file_upload`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '3', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-11 07:37:53'),
(2, '1', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-09 08:19:21'),
(3, '1', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-09 08:19:21'),
(4, '4', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-11 07:38:12'),
(5, '1', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-09 08:19:21'),
(6, '1', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-09 08:19:21'),
(7, '2', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-11 07:38:05'),
(8, '3', 'Kebangkitan Kaum Janda', 'images.jpg', 'Supri Legend', '2002', 'Ya', '10012', 'tes.pdf', '1', '2024-01-09 08:19:21', '2024-01-11 07:37:46'),
(9, '2', 'Pemikat Janda', '269005264-288-k914795.jpg', 'Supri Legend', '2000', 'Ya', '10012', 'MOCK UP TOPI ASSHODRIYYAH.pdf', '1', '2024-01-09 09:01:38', '2024-01-09 09:01:38');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_library_category`
--

CREATE TABLE `tbl_library_category` (
  `id_category` bigint(20) UNSIGNED NOT NULL,
  `category_code` varchar(255) NOT NULL,
  `category_type` varchar(200) DEFAULT NULL,
  `category_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_library_category`
--

INSERT INTO `tbl_library_category` (`id_category`, `category_code`, `category_type`, `category_name`, `created_at`, `updated_at`) VALUES
(1, '1', 'reading', 'Module', '2024-01-09 08:16:36', '2024-01-09 08:16:36'),
(2, '2', 'reading', 'Materi', '2024-01-09 08:16:45', '2024-01-09 08:16:45'),
(3, '3', 'reading', 'Jurnal', '2024-01-09 08:16:52', '2024-01-09 08:16:52'),
(4, '4', 'reading', 'Books', '2024-01-09 08:17:06', '2024-01-09 08:17:06'),
(5, '5', 'videos', 'Video Pembelajaran', '2024-01-11 07:40:10', '2024-01-11 07:40:10'),
(6, '6', 'videos', 'Podcast TaxsamTalks', '2024-01-11 07:40:10', '2024-01-11 07:40:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_library_video`
--

CREATE TABLE `tbl_library_video` (
  `id_library_video` bigint(20) UNSIGNED NOT NULL,
  `category_code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url_video` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `created_by` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_library_video`
--

INSERT INTO `tbl_library_video` (`id_library_video`, `category_code`, `title`, `url_video`, `cover`, `created_by`, `created_at`, `updated_at`) VALUES
(1, '5', 'Tutorial', 'https://drive.google.com/file/d/16kZcjHdasXj_svRnZJt6tRvf8WkFVKub/preview', '269005264-288-k914795.jpg', '1', '2024-01-11 07:41:14', '2024-01-11 07:41:14'),
(2, '6', 'Tutorial Memikat Janda', 'https://drive.google.com/file/d/16kZcjHdasXj_svRnZJt6tRvf8WkFVKub/preview', '269005264-288-k914795.jpg', '1', '2024-01-11 07:41:14', '2024-01-11 07:41:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tax_base`
--

CREATE TABLE `tbl_tax_base` (
  `id_tax_base` bigint(20) UNSIGNED NOT NULL,
  `tax_base_name` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_tax_base`
--

INSERT INTO `tbl_tax_base` (`id_tax_base`, `tax_base_name`, `flag`, `created_by`, `created_at`, `updated_at`) VALUES
(3, 'Base A', 1, 1, '2024-01-16 02:42:18', '2024-01-16 02:42:18'),
(4, 'Base B', 2, 1, '2024-01-16 02:42:24', '2024-01-16 02:42:24'),
(5, 'Base C', 3, 1, '2024-01-16 02:42:30', '2024-01-16 02:42:30');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tax_base_detail`
--

CREATE TABLE `tbl_tax_base_detail` (
  `id_tax_base_detail` bigint(20) UNSIGNED NOT NULL,
  `id_tax_base` int(11) NOT NULL,
  `id_tax_category` int(11) NOT NULL,
  `id_tax_type` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `sort_description` longtext NOT NULL,
  `detail_description` longtext NOT NULL,
  `year` year(4) NOT NULL,
  `country` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_tax_base_detail`
--

INSERT INTO `tbl_tax_base_detail` (`id_tax_base_detail`, `id_tax_base`, `id_tax_category`, `id_tax_type`, `title`, `number`, `sort_description`, `detail_description`, `year`, `country`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 3, 1, 1, 'Title A', '10000', 'Sort Description A', '<p style=\"text-align: center; \"><span style=\"font-weight: 700;\"><u>Detail Description</u> A</span><br></p>', '2024', 'Konoha', 1, '2024-01-16 02:52:12', '2024-01-16 02:52:12'),
(2, 3, 2, 2, 'Title B', '11000', 'Sort Description B', '<p style=\"text-align: center; \"><span style=\"font-weight: 700;\"><u>Detail Description</u> A</span><br></p>', '2024', 'Konoha', 1, '2024-01-16 02:52:12', '2024-01-16 02:52:12');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tax_category`
--

CREATE TABLE `tbl_tax_category` (
  `id_tax_category` bigint(20) UNSIGNED NOT NULL,
  `tax_category_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_tax_category`
--

INSERT INTO `tbl_tax_category` (`id_tax_category`, `tax_category_name`, `created_at`, `updated_at`) VALUES
(1, 'PBB', '2024-01-13 03:57:25', '2024-01-13 03:58:24'),
(2, 'BPHTB', '2024-01-13 03:57:32', '2024-01-13 03:58:09'),
(3, 'Lainnya', '2024-01-13 03:57:50', '2024-01-13 03:57:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tax_type`
--

CREATE TABLE `tbl_tax_type` (
  `id_tax_type` bigint(20) UNSIGNED NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_tax_type`
--

INSERT INTO `tbl_tax_type` (`id_tax_type`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'Type A', '2024-01-13 14:11:13', NULL),
(2, 'Type B', '2024-01-16 02:40:34', '2024-01-16 02:40:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `role` varchar(50) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `avatar` longtext DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `role`, `email`, `email_verified_at`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', 'admin', 'megakusumadewa@gmail.com', '2024-01-09 12:40:40', '$2y$10$WioGQ6B65wCwbNeLUuKC2ulWI.6Ns7IanwYqVQPlgM5aEfWfwYfB6', 'janda.jpg', NULL, '2024-01-09 12:40:40', NULL),
(2, 'users', 'Users', 'users', 'darkk235@gmail.com', '2024-01-09 12:40:40', '$2y$10$nX/RfKieBt3ZppVKVimU8ufhhIrBZXT38QZFy91/zxyQQc2Zt1gle', 'janda.jpg', NULL, '2024-01-09 12:40:40', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `tbl_library_book`
--
ALTER TABLE `tbl_library_book`
  ADD PRIMARY KEY (`id_library_book`);

--
-- Indexes for table `tbl_library_category`
--
ALTER TABLE `tbl_library_category`
  ADD PRIMARY KEY (`id_category`);

--
-- Indexes for table `tbl_library_video`
--
ALTER TABLE `tbl_library_video`
  ADD PRIMARY KEY (`id_library_video`);

--
-- Indexes for table `tbl_tax_base`
--
ALTER TABLE `tbl_tax_base`
  ADD PRIMARY KEY (`id_tax_base`);

--
-- Indexes for table `tbl_tax_base_detail`
--
ALTER TABLE `tbl_tax_base_detail`
  ADD PRIMARY KEY (`id_tax_base_detail`);

--
-- Indexes for table `tbl_tax_category`
--
ALTER TABLE `tbl_tax_category`
  ADD PRIMARY KEY (`id_tax_category`);

--
-- Indexes for table `tbl_tax_type`
--
ALTER TABLE `tbl_tax_type`
  ADD PRIMARY KEY (`id_tax_type`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_library_book`
--
ALTER TABLE `tbl_library_book`
  MODIFY `id_library_book` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_library_category`
--
ALTER TABLE `tbl_library_category`
  MODIFY `id_category` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_library_video`
--
ALTER TABLE `tbl_library_video`
  MODIFY `id_library_video` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_tax_base`
--
ALTER TABLE `tbl_tax_base`
  MODIFY `id_tax_base` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_tax_base_detail`
--
ALTER TABLE `tbl_tax_base_detail`
  MODIFY `id_tax_base_detail` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_tax_category`
--
ALTER TABLE `tbl_tax_category`
  MODIFY `id_tax_category` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_tax_type`
--
ALTER TABLE `tbl_tax_type`
  MODIFY `id_tax_type` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
