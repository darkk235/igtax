<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_tax_base_detail', function (Blueprint $table) {
            $table->id('id_tax_base_detail');
            $table->integer('id_tax_base');
            $table->integer('id_tax_category');
            $table->integer('id_tax_type');
            $table->string('title');
            $table->string('number');
            $table->longText('sort_description');
            $table->longText('detail_description');
            $table->year('year');
            $table->string('country');
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_tax_base_detail');
    }
};
