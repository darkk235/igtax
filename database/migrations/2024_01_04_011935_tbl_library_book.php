<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_library_book', function (Blueprint $table) {
            $table->id('id_library_book');
            $table->string('category_code');
            $table->string('title');
            $table->string('cover');
            $table->string('author');
            $table->string('publication_year');
            $table->string('publisher');
            $table->string('isbn');
            $table->string('file_upload');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_library_book');
    }
};
