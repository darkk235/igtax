<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = Category::query();

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $edit = "<a href='" . route('library.category.edit', $row->id_category) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('library.category.delete', $row->id_category) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = "Category";
        $data['active'] = "category";
        $data['menu'] = "library";

        return view('admin.library.category.index', $data);
    }

    public function add()
    {
        $data['title'] = "Add Category";
        $data['active'] = "category";
        $data['menu'] = "library";

        return view('admin.library.category.add', $data);
    }

    public function edit(Request $request)
    {
        $data['title'] = "Edit Category";
        $data['active'] = "category";
        $data['menu'] = "library";
        $data['category'] = Category::find($request->ids);

        return view('admin.library.category.edit', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_code' => 'required',
            'category_name' => 'required',
            'category_type' => 'required'
        ]);

        Category::create($request->input());

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function update(Request $request)
    {
        $request->validate([
            'category_code' => 'required',
            'category_name' => 'required',
        ]);

        Category::find($request->ids)->update($request->input());

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function destroy(Request $request)
    {
        Category::find($request->ids)->delete();

        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
