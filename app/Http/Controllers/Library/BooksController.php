<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class BooksController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = Books::query()->with('users');

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('createdBy',function($row){
                    return $row->users->name;
                })
                ->addColumn('action', function ($row) {
                    $edit = "<a href='" . route('library.books.edit', $row->id_library_book) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('library.books.delete', $row->id_library_book) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = "Books";
        $data['active'] = "books";
        $data['menu'] = "library";

        return view('admin.library.books.index', $data);
    }

    public function add()
    {
        $data['title'] = "Add Books";
        $data['active'] = "books";
        $data['menu'] = "library";
        $data['category'] = Category::all();

        return view('admin.library.books.add', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_code' => 'required',
            'title' => 'required',
            'cover' => ['required','mimes:png,jpg','image'],
            'author' => 'required',
            'publication_year' => 'required',
            'publisher' => 'required',
            'isbn' => 'required',
            'file_upload' => ['required','file','mimes:pdf'],
        ]);

        $file = $request->file('file_upload');
        $cover = $request->file('cover');
        
        $fileName = $file->getClientOriginalName();
        $coverName = $cover->getClientOriginalName();
        $path = 'books/';

        Books::create($request->except('file_upload','cover') + ['file_upload' => $fileName,'cover' => $coverName,'created_by' => Auth::user()->id]);

        $file->move($path, $fileName);
        $cover->move($path, $coverName);

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function edit(Request $request)
    {
        $data['title'] = "Edit Books";
        $data['active'] = "books";
        $data['menu'] = "library";
        $data['category'] = Category::all();
        $data['books'] = Books::find($request->ids);

        return view('admin.library.books.edit', $data);
    }

    public function update(Request $request)
    {
        try {
            $request->validate([
                'category_code' => 'required',
                'title' => 'required',
                'cover' => ['nullable','mimes:png,jpg','image'],
                'author' => 'required',
                'publication_year' => 'required',
                'publisher' => 'required',
                'isbn' => 'required',
                'file_upload' => ['nullable','mimes:pdf'],
            ]);
    
            $path = 'Books/';
            $model = Books::find($request->ids);
    
            if ($file = $request->file('file_upload')) {
                $fileName = $file->getClientOriginalName();
                unlink($path.$model->file_upload);
                $file->move($path, $fileName);
                $saveFile = $fileName;
            }else{
                $saveFile = $model->file_upload;
            }
    
            if ($cover = $request->file('cover')) {
                $coverName = $cover->getClientOriginalName();
                unlink($path.$model->cover);
                $cover->move($path, $coverName);
                $saveCover = $coverName;
            }else{
                $saveCover = $model->cover;
            }
    
            $model->update($request->except('file_upload','cover') + ['file_upload' => $saveFile,'cover' => $saveCover]);
    
            return Redirect::back()->with('success', 'Save Successfully !');
        } catch (\Throwable $th) {
            return Redirect::back()->with('error',$th->getMessage());
        }
    }

    public function destroy(Request $request)
    {
        $model = Books::find($request->ids);
        $path = 'Books/';
        unlink($path.$model->file_upload);
        unlink($path.$model->cover);

        $model->delete();

        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
