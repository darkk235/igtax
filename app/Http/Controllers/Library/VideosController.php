<?php

namespace App\Http\Controllers\Library;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Videos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class VideosController extends Controller
{
    public function index(Request $request){
        if ($request->ajax()) {
            $model = Videos::query();

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('createdBy',function($row){
                    return $row->users->name;
                })
                ->addColumn('action', function ($row) {
                    $edit = "<a href='" . route('library.videos.edit', $row->id_library_video) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('library.videos.delete', $row->id_library_video) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = "Videos";
        $data['active'] = "videos";
        $data['menu'] = "library";

        return view('admin.library.videos.index',$data);
    }

    public function add(){
        $data['title'] = "Add Video";
        $data['active'] = "videos";
        $data['menu'] = "library";
        $data['category'] = Category::all();

        return view('admin.library.videos.add',$data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_code' => 'required',
            'title' => 'required',
            'url_video' => ['required','url']
        ]);

        Videos::create(['created_by' => Auth::user()->id]);

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function edit(Request $request){
        $data['title'] = "Edit Video";
        $data['active'] = "videos";
        $data['menu'] = "library";
        $data['category'] = Category::all();
        $data['video'] = Videos::find($request->ids);

        return view('admin.library.videos.edit',$data);
    }

    public function update(Request $request)
    {
        try {
            $request->validate([
                'category_code' => 'required',
                'title' => 'required',
                'url_video' => ['required','url']
            ]);

            Videos::find($request->ids)->update($request->input());

            return Redirect::back()->with('success', 'Save Successfully !');
        } catch (\Throwable $th) {
            return Redirect::back()->with('error',$th->getMessage());
        }
    }

    public function destroy(Request $request)
    {
        $model = Videos::find($request->ids)->delete();
        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
