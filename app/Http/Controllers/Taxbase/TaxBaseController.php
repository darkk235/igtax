<?php

namespace App\Http\Controllers\Taxbase;

use App\Http\Controllers\Controller;
use App\Models\TaxBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class TaxBaseController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = TaxBase::query()->with('users');

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('createdBy',function($row){
                    return $row->users->name;
                })
                ->addColumn('action', function ($row) {
                    $view = "<a href='" . route('tax.detail', $row->id_tax_base) . "' type='button' class='btn btn-sm btn-info'><i class='fas fa-eye'></i></a>";
                    $edit = "<a href='" . route('tax.base.edit', $row->id_tax_base) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('tax.base.delete', $row->id_tax_base) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $view . " " . $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = 'Tax Base';
        $data['active'] = 'taxbase';
        $data['menu'] = 'tax';

        return view('admin.tax.base.index', $data);
    }

    public function add(Request $request)
    {
        $data['title'] = 'Add Base';
        $data['active'] = 'taxbase';
        $data['menu'] = 'tax';

        return view('admin.tax.base.add', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'flag' => 'required',
            'tax_base_name' => 'required',
        ]);

        TaxBase::create($request->input() + ['created_by' => Auth::user()->id]);

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function edit(Request $request)
    {
        $data['title'] = 'Edit Base';
        $data['active'] = 'taxbase';
        $data['menu'] = 'tax';
        $data['taxbase'] = TaxBase::find($request->ids);

        return view('admin.tax.base.edit', $data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'flag' => 'required',
            'tax_base_name' => 'required',
        ]);

        TaxBase::find($request->ids)->update($request->input());

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function destroy(Request $request)
    {
        TaxBase::find($request->ids)->delete();

        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
