<?php

namespace App\Http\Controllers\Taxbase;

use App\Http\Controllers\Controller;
use App\Models\TaxBase;
use App\Models\TaxCategory;
use App\Models\TaxDetail;
use App\Models\TaxType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class TaxDetailController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = TaxDetail::query()->with('users')->where('id_tax_base', $request->ids);

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('createdBy', function ($row) {
                    return $row->users->name;
                })
                ->addColumn('action', function ($row) {
                    $edit = "<a href='" . route('tax.detail.edit', $row->id_tax_base_detail) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('tax.detail.delete', $row->id_tax_base_detail) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = 'Tax Base Detail';
        $data['active'] = 'taxbase';
        $data['menu'] = 'tax';
        $data['ids'] = $request->ids;

        return view('admin.tax.detail.index', $data);
    }

    public function add(Request $request)
    {
        $data['title'] = 'Add Tax Detail';
        $data['active'] = 'taxbase';
        $data['menu'] = 'tax';
        $data['taxbase'] = TaxBase::find($request->ids);
        $data['category'] = TaxCategory::all();
        $data['type'] = TaxType::all();

        return view('admin.tax.detail.add', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'id_tax_category' => 'required',
            'id_tax_type' => 'required',
            'title' => 'required',
            'number' => ['required'],
            'sort_description' => 'required',
            'detail_description' => 'required',
            'year' => ['required'],
            'country' => ['required'],
        ]);

        TaxDetail::create($request->except('id_tax_type') + ['created_by' => Auth::user()->id, 'id_tax_base' => $request->ids, 'id_tax_type' => $request->id_tax_type]);

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function edit(Request $request)
    {
        $data['title'] = 'Edit Tax Detail';
        $data['active'] = 'taxbase';
        $data['menu'] = 'tax';
        $data['taxdetail'] = TaxDetail::find($request->ids);
        $data['category'] = TaxCategory::all();
        $data['type'] = TaxType::all();

        return view('admin.tax.detail.edit', $data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'id_tax_category' => 'required',
            'id_tax_type' => 'required',
            'title' => 'required',
            'number' => ['required'],
            'sort_description' => 'required',
            'detail_description' => 'required',
            'year' => ['required'],
            'country' => ['required','unique:tbl_tax_base_detail,country,'.$request->id_tax_type.',id_tax_base_detail'],
        ]);

        TaxDetail::find($request->ids)->update($request->except('id_tax_type') + ['id_tax_type' => $request->id_tax_type]);

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function destroy(Request $request)
    {
        TaxDetail::find($request->ids)->delete();

        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
