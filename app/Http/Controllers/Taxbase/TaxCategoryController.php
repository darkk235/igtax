<?php

namespace App\Http\Controllers\Taxbase;

use App\Http\Controllers\Controller;
use App\Models\Taxbase;
use App\Models\TaxCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class TaxCategoryController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = TaxCategory::query();

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $edit = "<a href='" . route('tax.category.edit', $row->id_tax_category) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('tax.category.delete', $row->id_tax_category) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = 'Tax Base Category';
        $data['active'] = 'taxcategory';
        $data['menu'] = 'tax';

        return view('admin.tax.category.index', $data);
    }

    public function add(Request $request)
    {
        $data['title'] = 'Add Category';
        $data['active'] = 'taxcategory';
        $data['menu'] = 'tax';

        return view('admin.tax.category.add', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'tax_category_name' => 'required',
        ]);

        TaxCategory::create($request->input());

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function edit(Request $request)
    {
        $data['title'] = 'Edit Category';
        $data['active'] = 'taxcategory';
        $data['menu'] = 'tax';
        $data['taxcategory'] = TaxCategory::find($request->ids);

        return view('admin.tax.category.edit', $data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'tax_category_name' => 'required',
        ]);

        TaxCategory::find($request->ids)->update($request->input());

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function destroy(Request $request)
    {
        TaxCategory::find($request->ids)->delete();

        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
