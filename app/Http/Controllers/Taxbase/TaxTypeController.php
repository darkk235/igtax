<?php

namespace App\Http\Controllers\Taxbase;

use App\Http\Controllers\Controller;
use App\Models\TaxType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\Facades\DataTables;

class TaxTypeController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = TaxType::query();

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $edit = "<a href='" . route('tax.type.edit', $row->id_tax_type) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('tax.type.delete', $row->id_tax_type) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = 'Tax Type Regulation';
        $data['active'] = 'taxtype';
        $data['menu'] = 'tax';

        return view('admin.tax.type.index', $data);
    }

    public function add(Request $request)
    {
        $data['title'] = 'Add Type Regulation';
        $data['active'] = 'taxtype';
        $data['menu'] = 'tax';

        return view('admin.tax.type.add', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'type_name' => 'required',
        ]);

        TaxType::create($request->input());

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function edit(Request $request)
    {
        $data['title'] = 'Add Type Regulation';
        $data['active'] = 'taxtype';
        $data['menu'] = 'tax';
        $data['taxtype'] = TaxType::find($request->ids);

        return view('admin.tax.type.edit', $data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'type_name' => 'required',
        ]);

        TaxType::find($request->ids)->update($request->input());

        return Redirect::back()->with('success', 'Save Successfully !');
    }

    public function destroy(Request $request)
    {
        TaxType::find($request->ids)->delete();

        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
