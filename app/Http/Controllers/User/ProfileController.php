<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class ProfileController extends Controller
{
    public function edit(Request $request): View
    {
        $data['title'] = "Edit Users";
        $data['active'] = "dashboard";
        $data['users'] = $request->user();

        return view('users.profile.edit', $data);
    }

    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->except('avatar'));

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        if ($file = $request->file('avatar')) {
            $fileName = $file->getClientOriginalName();
            $file->move('avatar',$fileName);
            $request->user()->avatar = $fileName;
        }

        $request->user()->save();

        return Redirect::route('users.edit',$request->ids)->with('success', 'Save Successfuly !');
    }

    public function destroy(Request $request)
    {
        $model = User::find($request->ids);
        unlink('avatar/'.$model->avatar);
        $model->delete();
        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
