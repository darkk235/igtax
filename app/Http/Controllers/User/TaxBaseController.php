<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\TaxBase;
use App\Models\TaxCategory;
use App\Models\TaxDetail;
use App\Models\TaxType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class TaxBaseController extends Controller
{
    public function index(Request $request)
    {
        $data['title'] = "TaxBase";
        $data['active'] = "taxbase";
        $data['menu'] = "taxbase";
        $data['taxbase'] = TaxBase::all();

        return view('users.taxbase.index', $data);
    }

    public function show(Request $request)
    {
        $taxdetail = TaxDetail::query()->with('category');
        $taxbase = TaxBase::find($request->id_tax_base);

        if ($request->ajax()) {
            return $this->jsonRequest($taxdetail,$request);
        }

        if ($taxbase->flag == 1) {
            return $this->peraturanShow($taxdetail,$request);
        }elseif ($taxbase->flag == 2) {
            return $this->countryShow($taxdetail,$request);
        }else{
            return $this->putusanShow($taxdetail,$request);
        }
    }

    private function peraturanShow ($taxdetail,$request) {
        $data['title'] = "PERATURAN";
        $data['active'] = "taxbase";
        $data['menu'] = "taxbase";
        $data['taxdetail'] = $taxdetail->where('id_tax_base',$request->id_tax_base)->paginate(2);
        $data['taxbase'] = TaxBase::find($request->id_tax_base);
        $data['taxcategory'] = TaxCategory::query()->withCount('detail')->get();
        $data['taxtype'] = TaxType::query()->withCount('detail')->get();

        return view('users.taxbase.peraturan', $data);
    }

    private function countryShow ($taxdetail,$request) {
        $data['title'] = "TAX TREATY";
        $data['active'] = "taxbase";
        $data['menu'] = "taxbase";
        $data['taxdetail'] = $taxdetail->get();
        $data['taxbase'] = TaxBase::find($request->id_tax_base);

        return view('users.taxbase.country', $data);
    }

    private function putusanShow ($taxdetail,$request) {
        $data['title'] = "PUTUSAN";
        $data['active'] = "taxbase";
        $data['menu'] = "taxbase";
        $data['taxdetail'] = $taxdetail->where('id_tax_base',$request->id_tax_base)->paginate(2);
        $data['taxbase'] = TaxBase::find($request->id_tax_base);
        $data['taxcategory'] = TaxCategory::query()->withCount('detail')->get();
        $data['taxtype'] = TaxType::query()->withCount('detail')->get();

        return view('users.taxbase.putusan', $data);
    }

    private function jsonRequest ($taxdetail,$request) {
        if ($type = $request->types) {
            $taxdetail->whereIn('id_tax_type', $type);
        }

        if ($category = $request->categorys) {
            $taxdetail->whereIn('id_tax_category', $category);
        }

        if ($date_from = $request->date_froms && $date_to = $request->date_tos) {
            $taxdetail->whereBetween('created_at', [$date_from, $date_to]);
        }

        if ($number = $request->numbers) {
            $taxdetail->where('number', 'like', '%' . $number . '%');
        }

        if ($search_name = $request->search_names) {
            $taxdetail->where('title', 'like', '%' . $search_name . '%');
        }

        return Response::json(['taxdetail' => $taxdetail->paginate(2)]);
    }

    public function reading(Request $request)
    {
        $taxdetail = TaxDetail::find($request->id_tax_base_detail);
        $data['title'] = "TaxBase Reading";
        $data['active'] = "taxbase";
        $data['menu'] = "taxbase";
        $data['taxdetail'] = $taxdetail;
        $data['terkait'] = TaxDetail::where('id_tax_type', $taxdetail['id_tax_type'])->get();
        return view('users.taxbase.read.peraturan', $data);
    }

    public function country(Request $request)
    {
        $taxdetail = TaxDetail::find($request->id_tax_base_detail);
        $data['title'] = "TaxBase Reading";
        $data['active'] = "taxbase";
        $data['menu'] = "taxbase";
        $data['taxdetail'] = $taxdetail;
        $data['terkait'] = TaxDetail::where('id_tax_type', $taxdetail['id_tax_type'])->get();
        return view('users.taxbase.read.country', $data);
    }

    public function countrys (Request $request) {
        $taxdetail = TaxDetail::query()->with('category');

        if ($request->method() == "POST") {
            $taxdetail->where('country','like','%'.$request->country.'%');
        }

        $data['title'] = "TAX TREATY";
        $data['active'] = "taxbase";
        $data['menu'] = "taxbase";
        $data['taxdetail'] = $taxdetail->get();
        $data['taxbase'] = TaxBase::find($request->id_tax_base);

        return view('users.taxbase.country', $data);
    }
}
