<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Books;
use App\Models\Category;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function reading(Request $request)
    {
        $data['title'] = "Reading";
        $data['active'] = "reading";
        $data['menu'] = "library";
        $data['category'] = Category::with('books')->where('category_type','reading')->get();

        return view('users.library.reading', $data);
    }

    public function codex(Request $request)
    {
        $data['title'] = "Videos";
        $data['active'] = "codex";
        $data['menu'] = "library";
        $data['category'] = Category::with('videos')->where('category_type','videos')->get();

        return view('users.library.videos', $data);
    }
}
