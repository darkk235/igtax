<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $data['title'] = "Dashboard";
        $data['active'] = "dashboard";

        return view('users.dashboard',$data);
    }
}
