<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $model = User::query();

            return DataTables::eloquent($model)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $edit = "<a href='" . route('users.edit', $row->id) . "' type='button' class='btn btn-sm btn-warning'><i class='fas fa-edit'></i></a>";
                    $delete = "<a href='" . route('users.delete', $row->id) . "' type='button' class='btn btn-sm btn-danger delete'><i class='fas fa-trash'></i></a>";
                    return $edit . " " . $delete;
                })
                ->toJson();
        }

        $data['title'] = "Users";
        $data['active'] = "users";

        return view('admin.users.index', $data);
    }

    public function edit(Request $request): View
    {
        $data['title'] = "Edit Users";
        $data['active'] = "users";
        $data['users'] = User::find($request->ids);

        return view('admin.users.edit', $data);
    }

    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->except('avatar'));

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        if ($file = $request->file('avatar')) {
            $fileName = $file->getClientOriginalName();
            $file->move('avatar',$fileName);
            $request->user()->avatar = $fileName;
        }

        $request->user()->save();

        return Redirect::route('users.edit',$request->ids)->with('success', 'Save Successfuly !');
    }

    public function destroy(Request $request)
    {
        $model = User::find($request->ids);
        unlink('avatar/'.$model->avatar);
        $model->delete();
        return Response::json(['icon' => 'success', 'message' => 'Delete Successfully !']);
    }
}
