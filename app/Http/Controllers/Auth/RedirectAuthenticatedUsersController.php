<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class RedirectAuthenticatedUsersController extends Controller
{
    public function home()
    {
        if (auth()->user()->role == 'admin') {
            return Redirect::route('admin.dashboard');
        }
        elseif(auth()->user()->role == 'users'){
            return Redirect::route('users.dashboard');
        }
        else{
            return auth()->logout();
        }
    }
}
