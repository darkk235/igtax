<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Videos extends Model
{
    protected $guarded = ['id_library_video'];
    protected $primaryKey = 'id_library_video';
    protected $table = 'tbl_library_video';

    public function users () : HasOne {
        return $this->hasOne(User::class,'id','created_by');
     }
}
