<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TaxCategory extends Model
{
    protected $guarded = ['id_tax_category'];
    protected $primaryKey = 'id_tax_category';
    protected $table = 'tbl_tax_category';

    public function detail() : HasMany {
        return $this->hasMany(TaxDetail::class,'id_tax_category','id_tax_category');
    }
}
