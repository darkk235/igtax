<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Category extends Model
{
    protected $guarded = ['id_category'];
    protected $primaryKey = 'id_category';
    protected $table = 'tbl_library_category';

    public function books () : HasMany  { 
        return $this->hasMany(Books::class,'category_code','category_code');
    }

    public function videos () : HasMany  { 
        return $this->hasMany(Videos::class,'category_code','category_code');
    }
}
