<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TaxBase extends Model
{
    protected $guarded = ['id_tax_base'];
    protected $primaryKey = 'id_tax_base';
    protected $table = 'tbl_tax_base';

    public function users () : HasOne {
        return $this->hasOne(User::class,'id','created_by');
     }
}
