<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Books extends Model
{
    protected $guarded = ['id_library_book'];
    protected $primaryKey = 'id_library_book';
    protected $table = 'tbl_library_book';

    public function users () : HasOne {
       return $this->hasOne(User::class,'id','created_by');
    }
}
