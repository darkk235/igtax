<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TaxDetail extends Model
{
    protected $guarded = ['id_tax_base_detail'];
    protected $primaryKey = 'id_tax_base_detail';
    protected $table = 'tbl_tax_base_detail';
    // protected $casts = [
    //     'id_tax_category' => 'json',
    // ];
    public function users () : HasOne {
        return $this->hasOne(User::class,'id','created_by');
     }

     public function category () : HasOne {
        return $this->hasOne(TaxCategory::class,'id_tax_category','id_tax_category');
     }
}
