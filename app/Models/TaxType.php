<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class TaxType extends Model
{
    protected $guarded = ['id_tax_type'];
    protected $primaryKey = 'id_tax_type';
    protected $table = 'tbl_tax_type';

    public function detail() : HasMany {
        return $this->hasMany(TaxDetail::class,'id_tax_type','id_tax_type');
    }
}
