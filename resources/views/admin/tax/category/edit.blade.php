@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">{{$title}}</div>
    <form action="{{route('tax.category.update',$taxcategory['id_tax_category'])}}" method="post">
        @method('PUT')
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-12 form-group">
                    <label for="tax_category_name">Name</label>
                    <input type="text" name="tax_category_name" class="form-control" value="{{$taxcategory['tax_category_name']}}">
                    @error('tax_category_name') <small class="text-danger">{{$message}}</small> @enderror
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button type="submit" class="btn btn-sm btn-primary mr-3">Submit</button>
            <a href="{{ route('tax.category') }}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </form>
</div>
@endsection