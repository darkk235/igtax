@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">{{$title}}</div>
    <form action="{{route('tax.base.store')}}" method="post">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-6 form-group">
                    <label for="tax_base_name">Name</label>
                    <input type="text" name="tax_base_name" class="form-control" value="{{old('tax_base_name')}}">
                    @error('tax_base_name') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="flag">Flag</label>
                    <select name="flag" class="form-control">
                        <option value="">-- Select Flag --</option>
                        <option value="1">Peraturan</option>
                        <option value="2">Negara</option>
                        <option value="3">Putusan</option>
                    </select>
                    @error('flag') <small class="text-danger">{{$message}}</small> @enderror
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button type="submit" class="btn btn-sm btn-primary mr-3">Submit</button>
            <a href="{{ route('tax.base') }}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </form>
</div>
@endsection
