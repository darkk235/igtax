@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">{{$title}}</div>
    <form action="{{route('tax.type.store')}}" method="post">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-12 form-group">
                    <label for="type_name">Name</label>
                    <input type="text" name="type_name" class="form-control" value="{{old('type_name')}}">
                    @error('type_name') <small class="text-danger">{{$message}}</small> @enderror
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button type="submit" class="btn btn-sm btn-primary mr-3">Submit</button>
            <a href="{{ route('tax.type') }}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </form>
</div>
@endsection