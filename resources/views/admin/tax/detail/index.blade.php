@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>
        <div class="card-tools">
            <a href="{{ route('tax.detail.add',$ids) }}" type="button" class="btn btn-sm"><i class="fas fa-plus"></i> New Tax Base</a>
            <a href="{{ route('tax.base') }}" type="button" class="btn btn-sm"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-sm table-bordered" id="dataTable" width="100%">
            <thead>
                <th>No</th>
                <th>Title</th>
                <th>Number</th>
                <th>Sort Description</th>
                <th>Created By</th>
                <th>Action</th>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('script')
<script>
    $('#dataTable').DataTable({
        serverside:true,
        processing:true,
        ajax:"{{route('tax.detail',$ids)}}",
        columnDefs: [
            { "className": "text-center","orderable":false, "targets": [ 0 , 5] }
        ],
        columns:[
            {data:'DT_RowIndex'},
            {data:'title'},
            {data:'number'},
            {data:'sort_description'},
            {data:'createdBy'},
            {data:'action'},
        ]
    });
</script>
@endpush