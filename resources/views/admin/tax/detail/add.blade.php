@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">{{$title}}</div>
    <form action="{{route('tax.detail.store',$taxbase['id_tax_base'])}}" method="post">
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-6 form-group">
                  <label for="id_tax_category">Category</label>
                  <select class="select2bs4" name="id_tax_category" data-placeholder="Select Category" style="width: 100%;">
                    <option value="">-- Select Category --</option>
                    @foreach ($category as $value)
                    <option {{old('id_tax_category') == $value['id_tax_category'] ? "selected" : ""}} value="{{$value['id_tax_category']}}">{{$value['tax_category_name']}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-6 form-group">
                    <label for="id_tax_type">Type</label>
                    <select name="id_tax_type" class="form-control select2">
                        <option value="">-- Select Type --</option>
                        @foreach ($type as $value)
                        <option {{old('id_tax_type') == $value['id_tax_type'] ? "selected" : ""}} value="{{$value['id_tax_type']}}">{{$value['type_name']}}</option>
                        @endforeach
                    </select>
                    @error('id_tax_type') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{old('title')}}">
                    @error('title') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="number">Number</label>
                    <input type="text" name="number" class="form-control" value="{{old('number')}}">
                    @error('number') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="year">Year</label>
                    <input type="number" name="year" class="form-control" value="{{old('year')}}">
                    @error('year') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="country">Country</label>
                    <input type="text" name="country" class="form-control" value="{{old('country')}}">
                    @error('country') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-12 form-group">
                    <label for="sort_description">Sort Description</label>
                    <textarea rows="2" type="text" name="sort_description" class="form-control">{{old('sort_description')}}</textarea>
                    @error('sort_description') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-12 form-group">
                    <label for="detail_description">Detail Description</label>
                    <textarea rows="4" type="text" id="detail_description" name="detail_description" class="form-control">{{old('detail_description')}}</textarea>
                    @error('detail_description') <small class="text-danger">{{$message}}</small> @enderror
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button type="submit" class="btn btn-sm btn-primary mr-3">Submit</button>
            <a href="{{ route('tax.detail',$taxbase['id_tax_base']) }}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </form>
</div>
@endsection

@push('script')
    <script>
        $('#detail_description').summernote({
            height: 450,
        })

         $('.select2bs4').select2({
            theme: 'bootstrap4'
        })
    </script>
@endpush    