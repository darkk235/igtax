@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>
    </div>
    <div class="card-body">
        <table class="table table-sm table-bordered" id="dataTable" width="100%">
            <thead>
                <th>No</th>
                <th>Name</th>
                <th>Permission</th>
                <th>Action</th>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('script')
<script>
    $('#dataTable').DataTable({
        serverside:true,
        processing:true,
        ajax:"{{route('users')}}",
        columnDefs: [
            { "className": "text-center","orderable":false, "targets": [ 0 , 3] }
        ],
        columns:[
            {data:'DT_RowIndex'},
            {data:'name'},
            {data:'role'},
            {data:'action'},
        ]
    });
</script>
@endpush