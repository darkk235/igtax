@extends('layouts.auth')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">{{$title}}</div>
            <form action="{{route('users.update',$users['id'])}}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img class="profile-user-img img-fluid img-circle" src="{{asset('avatar/'.$users['avatar'].'')}}" alt="User profile picture" style="width: 200px; height:200px;">
                    </div>

                    <h3 class="profile-username text-center">{{$users['name']}}</h3>

                    <p class="text-muted text-center">{{$users['permission']}}</p>

                    <div class="row mt-4">
                        <div class="form-group col-6">
                            <label for="name">Name</label>
                            <input type="text" name="name" value="{{$users['name']}}" class="form-control">
                            @error('name')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label for="username">Username</label>
                            <input type="text" name="username" value="{{$users['username']}}" class="form-control">
                            @error('username')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label for="email">Email</label>
                            <input type="email" name="email" value="{{$users['email']}}" class="form-control">
                            @error('email')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                        <div class="form-group col-6">
                            <label for="avatar">Photo</label>
                            <input type="file" name="avatar" class="form-control">
                            @error('avatar')<small class="text-danger">{{$message}}</small>@enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-center">
                    <button type="submit" class="btn btn-sm btn-primary mr-3">Submit</button>
                    <a href="{{route('users')}}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection