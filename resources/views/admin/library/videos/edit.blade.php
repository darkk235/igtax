@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">{{$title}}</div>
    <form action="{{route('library.videos.update',$video['id_library_video'])}}" method="post">
        <div class="card-body">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-6 form-group">
                    <label for="title">Category</label>
                    <select name="category_code" class="form-control">
                        <option value="">-- Select Category --</option>
                        @foreach ($category as $value)
                        <option {{$value['category_code'] == $video['category_code'] ? 'selected' : ''}} value="{{ $value['category_code'] }}">{{ $value['category_name'] }}</option>
                        @endforeach
                    </select>
                    @error('category_code') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{$video['category_code']}}">
                    @error('title') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-12 form-group">
                    <label for="url_video">URL Video</label>
                    <input type="text" name="url_video" class="form-control" value="{{$video['url_video']}}">
                    @error('url_video') <small class="text-danger">{{$message}}</small> @enderror
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button class="btn btn-sm btn-primary mr-3">Submit</button>
            <a href="{{ route('library.videos') }}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </form>
</div>
@endsection
