@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">{{$title}}</div>
    <form action="{{ route('library.books.update',$books['id_library_book']) }}" method="post" enctype="multipart/form-data">
        <div class="card-body">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-6 form-group">
                    <label for="title">Category</label>
                    <select name="category_code" class="form-control">
                        <option value="">-- Select Category --</option>
                        @foreach ($category as $value)
                        <option {{$value['category_code'] == $books['category_code'] ? 'selected' : ''}} value="{{ $value['category_code'] }}">{{ $value['category_name'] }}</option>
                        @endforeach
                    </select>
                    @error('category_code') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" value="{{$books['title']}}">
                    @error('title') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="author">Author</label>
                    <input type="text" name="author" class="form-control" value="{{$books['author']}}">
                    @error('author') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="publication_year">Pubication Year</label>
                    <input type="number" name="publication_year" class="form-control" value="{{$books['publication_year']}}">
                    @error('publication_year') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="publisher">Publisher</label>
                    <input type="text" name="publisher" class="form-control" value="{{$books['publisher']}}">
                    @error('publisher') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="isbn">ISBN</label>
                    <input type="text" name="isbn" class="form-control" value="{{$books['isbn']}}">
                    @error('isbn') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="file_upload">File</label>
                    <input type="file" name="file_upload" class="form-control" value="{{$books['file_upload']}}">
                    @error('file_upload') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="cover">Cover</label>
                    <input type="file" name="cover" class="form-control" value="{{$books['cover']}}">
                    @error('cover') <small class="text-danger">{{$message}}</small> @enderror
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button class="btn btn-sm btn-primary mr-2"><i class="fas fa-save"></i> Save</button>
            <a href="{{ route('library.books') }}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </form>
</div>
@endsection