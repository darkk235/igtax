@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>
        <div class="card-tools">
            <a href="{{ route('library.books.add') }}" type="button" class="btn btn-sm"><i class="fas fa-plus"></i> New Book</a>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-sm table-bordered" id="dataTable" width="100%">
            <thead>
                <th>No</th>
                <th>Title</th>
                <th>Author</th>
                <th>Created By</th>
                <th>Action</th>
            </thead>
        </table>
    </div>
</div>
@endsection

@push('script')
<script>
    $('#dataTable').DataTable({
        serverside:true,
        processing:true,
        ajax:"{{route('library.books')}}",
        columnDefs: [
            { "className": "text-center","orderable":false, "targets": [ 0 , 4] }
        ],
        columns:[
            {data:'DT_RowIndex'},
            {data:'title'},
            {data:'author'},
            {data:'createdBy'},
            {data:'action'},
        ]
    });
</script>
@endpush