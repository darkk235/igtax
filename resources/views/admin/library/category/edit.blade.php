@extends('layouts.auth')
@section('content')
<div class="card card-primary">
    <div class="card-header">{{$title}}</div>
    <form action="{{route('library.category.update',$category['id_category'])}}" method="post">
        @method('PUT')
        @csrf
        <div class="card-body">
            <div class="row">
                <div class="col-6 form-group">
                    <label for="category_code">Code</label>
                    <input type="text" name="category_code" class="form-control" value="{{$category['category_code']}}">
                    @error('category_code') <small class="text-danger">{{$message}}</small> @enderror
                </div>
                <div class="col-6 form-group">
                    <label for="category_name">Name</label>
                    <input type="text" name="category_name" class="form-control" value="{{$category['category_name']}}">
                    @error('category_name') <small class="text-danger">{{$message}}</small> @enderror
                </div>
            </div>
        </div>
        <div class="card-footer d-flex justify-content-center">
            <button class="btn btn-sm btn-primary mr-3">Submit</button>
            <a href="{{ route('library.category') }}" type="button" class="btn btn-sm btn-danger"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </form>
</div>
@endsection