<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="index3.html" class="brand-link">
        <img src="{{asset('adminlte3')}}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('avatar/'.Auth::user()->avatar.'')}}" style="width: 35px; height:35px;" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{Auth::user()->role == 'admin' ? route('users.edit',Auth::user()->id) : route('profile.edit',Auth::user()->id)}}" class="d-block">{{Auth::user()->name}}</a>
            </div>
        </div>

       @if (Auth::user()->role == 'admin')
       <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('admin.dashboard')}}" class="nav-link {{$active == "dashboard" ? "active" : ""}}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item {{!empty($menu) && $menu == "library" ? "menu-open" : "" }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>Library <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('library.category')}}" class="nav-link {{$active == "category" ? "active" : ""}}">
                                <i class="fas fa-clipboard nav-icon"></i>
                                <p>Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('library.books')}}" class="nav-link {{$active == "books" ? "active" : ""}}">
                                <i class="fas fa-book nav-icon"></i>
                                <p>Books</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('library.videos')}}" class="nav-link {{$active == "videos" ? "active" : ""}}">
                                <i class="fas fa-video nav-icon"></i>
                                <p>Videos</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{!empty($menu) && $menu  == "tax" ? "menu-open" : "" }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>IGTax <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('tax.category')}}" class="nav-link {{$active == "taxcategory" ? "active" : ""}}">
                                <i class="fas fa-clipboard nav-icon"></i>
                                <p>Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('tax.type')}}" class="nav-link {{$active == "taxtype" ? "active" : ""}}">
                                <i class="fas fa-book nav-icon"></i>
                                <p>Tax Type Regulation</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('tax.base')}}" class="nav-link {{$active == "taxbase" ? "active" : ""}}">
                                <i class="fas fa-book nav-icon"></i>
                                <p>Tax Base</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{route('users')}}" class="nav-link {{$active == "users" ? "active" : ""}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Users</p>
                    </a>
                </li>
            </ul>
        </nav>
       @endif

       @if (Auth::user()->role == 'users')
       <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{route('users.dashboard')}}" class="nav-link {{$active == "dashboard" ? "active" : ""}}">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-item {{!empty($menu) && $menu == "library" ? "menu-open" : "" }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>Library <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('library.reading')}}" class="nav-link {{$active == "reading" ? "active" : ""}}">
                                <i class="fas fa-clipboard nav-icon"></i>
                                <p>Reading</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('library.codex')}}" class="nav-link {{$active == "codex" ? "active" : ""}}">
                                <i class="fas fa-clipboard nav-icon"></i>
                                <p>Videos</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item {{!empty($menu) && $menu == "taxbase" ? "menu-open" : "" }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-folder"></i>
                        <p>TaxBase <i class="right fas fa-angle-left"></i></p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('taxbase')}}" class="nav-link {{$active == "taxbase" ? "active" : ""}}">
                                <i class="fas fa-clipboard nav-icon"></i>
                                <p>TaxBase IGTax</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
       @endif
    </div>
</aside>
