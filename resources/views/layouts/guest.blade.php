<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/dist/css/adminlte.min.css">
</head>

<body class="hold-transition login-page">
    <div class="login-box">

        <div class="card card-primary">
            <div class="card-header text-center">
                <a href="/" class="h1"><b>ADMIN LTE</b></a>
            </div>
            
            @yield('content')
        </div>
    </div>

    <script src="{{asset('adminlte3')}}/plugins/jquery/jquery.min.js"></script>
    <script src="{{asset('adminlte3')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('adminlte3')}}/dist/js/adminlte.min.js"></script>
</body>

</html>