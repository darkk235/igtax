<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/sweetalert2/sweetalert2.min.css"/>
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('adminlte3')}}/plugins/summernote/summernote-bs4.min.css">

    @yield('css')
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Navbar -->
        @include('layouts.navbar')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('layouts.sidebar')
        <!-- Main Sidebar Container -->

        <div class="content-wrapper">
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0">{{ config('app.name', 'Laravel') }}</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">{{ config('app.name', 'Laravel') }}</a></li>
                                <li class="breadcrumb-item active">{{$title}}</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="container-fluid">
                    @if (session('error'))
                    <div class="alert alert-danger alert-dismissible fade show">
                        <strong>{{session('error')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show">
                        <strong>{{session('success')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif

                    @yield('content')
                </div>
            </div>
        </div>

        <aside class="control-sidebar control-sidebar-dark">
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>

        <!-- Main Footer -->
        @include('layouts.footer')
         <!-- Main Footer -->
    </div>

    <script src="{{asset('adminlte3')}}/plugins/jquery/jquery.min.js"></script>
    <script src="{{asset('adminlte3')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('adminlte3')}}/dist/js/adminlte.min.js"></script>
    <script src="{{asset('adminlte3')}}/plugins/select2/js/select2.full.min.js"></script>
    <script src="{{asset('adminlte3')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('adminlte3')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('adminlte3')}}/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script src="{{asset('adminlte3')}}/plugins/summernote/summernote-bs4.min.js"></script>
    <script>
        swal_delete('.delete', '#dataTable')

        function toasts_success(icons, titles) {
                Swal.fire({
                    position: 'top-end',
                    icon: icons,
                    title: titles,
                    showConfirmButton: false,
                    timer: 1500
                });
            }

            function swal_delete(selector, table = false) {
                $(document).on('click', selector, function(e) {
                    e.preventDefault();
                    Swal.fire({
                        title: 'Are you sure?',
                        text: "You won't be able to revert this!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: "DELETE",
                                url: $(this).attr('href'),
                                headers: {
                                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                                },
                                dataType: "JSON",
                                success: function(res) {
                                    console.log(res);
                                    $(table).DataTable().ajax.reload(null, false);
                                    toasts_success(res.icon, res.message)
                                }
                            });
                        }
                    })
                })
            }
    </script>
    @stack('script')
</body>

</html>