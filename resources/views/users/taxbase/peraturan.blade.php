@extends('layouts.auth')
@section('content')
<div class="row">
    <div class="col-sm-3">
        <div class="card card-primary">
            <div class="card-header">Jenis Peraturan</div>
            <div class="card-body" style="overflow-y: scroll; height:180px">
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        @foreach ($taxtype as $value)
                        <li class="nav-item">
                            <div class="form-group nav-link p-0">
                                <input type="checkbox" class="mr-3 portugas" name="id_tax_type" value="{{$value['id_tax_type']}}">
                                <label class="text-muted mb-0">{{$value['type_name']}}</label>
                                <p><span class="right badge badge-danger">{{$value['detail_count']}}</span></p>
                                <div class="dropdown-divider"></div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">Kategori</div>
            <div class="card-body" style="overflow-y: scroll; height:180px">
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        @foreach ($taxcategory as $value)
                        <li class="nav-item">
                            <div class="form-group nav-link p-0">
                                <input type="checkbox" class="mr-3 portugas" name="id_tax_category" value="{{$value['id_tax_category']}}">
                                <label class="text-muted mb-0">{{$value['tax_category_name']}}</label>
                                <p><span class="right badge badge-danger">{{$value['detail_count']}}</span></p>
                                <div class="dropdown-divider"></div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>

        <div class="card card-primary">
            <div class="card-header">Filter</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Dari Tanggal</label>
                            <input class="form-control" type="date" name="date_from" id="date_from">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Ke Tanggal</label>
                            <input class="form-control" type="date" name="date_to" id="date_to">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Nomor</label>
                            <input class="form-control" type="text" name="number" id="number">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>Kata Kunci</label>
                            <input class="form-control" type="search" name="search_name" id="search_name">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button class="btn btn-block btn-sm btn-primary portugas">Cari</button>
                        <button id="reset" class="btn btn-block btn-sm btn-danger">Reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="contents">
            @foreach ($taxdetail as $value)
            <div class="card card-primary">
                <div class="card-header">{{$value->title}} Nomor : <span>{{$value->number}}</div>
                <div class="card-body">
                    <p class="text-muted mb-1">Kategori : {{$value->category->tax_category_name}}</p>
                    <p>{{$value->sort_description}}</p>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <p class="mr-5"><i class="fas fa-calendar"></i> {{date('d-M-Y',strtotime($value->created_at))}}</p>
                        <div class="mr-5"><a href="{{route('taxbase.reading',$value->id_tax_base_detail)}}"><i class="fas fa-eye"></i> View</a></div>
                        <p><i class="fas fa-share"></i> Share</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $(document).ready(function() {
        $('#reset').click(function(e){
            e.preventDefault()
            window.location.reload()
        })

        $(".portugas").on('click', function(e) {
            var type = [];
            var category = [];
            var date_from = $('#date_from').val()
            var date_to = $('#date_to').val()
            var number = $('#number').val()
            var search_name = $('#search_name').val()

            $.each($("input[type=checkbox]:checked"), function() {
                var name = $(this).attr('name')

                if (name == 'id_tax_type') {
                    var val = $(this).val()
                    type.push(val);
                }

                if (name == 'id_tax_category') {
                    var val = $(this).val()
                    category.push(val);
                }
            });

            $.ajax({
                url: "{{route('taxbase.show',$taxbase['id_tax_base'])}}",
                type: 'GET',
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: "JSON",
                data: {
                    categorys: category,
                    types: type,
                    date_froms: date_from,
                    date_tos: date_to,
                    numbers: number,
                    search_names: search_name,
                },
                success: function(res) {
                    console.log(res);
                    $('.contents').empty()
                    var data = res.taxdetail.data;
                    for (let index = 0; index < data.length; index++) {
                        var html =
                            '<div class="card card-primary">' +
                            '<div class="card-header">' + data[index].title + ' Nomor : ' + data[index].number + '</div>' +
                            '<div class="card-body">' +
                            '<div class="text-muted">Kategori : ' + data[index].category.tax_category_name + '</div>' +
                            '<div class="mt-1"><p class="mb-0">' + data[index].sort_description + '</p></div>' +
                            '</div>' +
                            '<div class="card-footer">' +
                            '<div class="row text-center">' +
                            '<div class="mr-5"><i class="fas fa-calendar"></i> ' + data[index].created_at + '</div>' +
                            '<div class="mr-5"><a href="<?= url('taxbase/reading') ?>/' + data[index].id_tax_base_detail + '"><i class="fas fa-eye"></i> View</a></div>' +
                            '<div class="mr-5"><i class="fas fa-share"></i> Share</div>' +
                            '</div>' +
                            '</div>'

                        $('.contents').append(html)
                    }

                }
            })
        });

    });
</script>
@endpush