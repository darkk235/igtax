@extends('layouts.auth')
@section('content')
<div class="row">
    <div class="col-sm-3">
        <div class="card">
            <div class="card-header">Cari Negara</div>
            <div class="card-body">
                <form action="{{route('taxbase.countrys',$taxbase['id_tax_base'])}}" method="POST">
                    @csrf
                    <input type="search" class="form-control" name="country" value="{{old('country')}}">
                    <button type="submit" class="btn btn-block btn-sm btn-primary mt-3">Cari </button>
                </form>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="card">
            <div class="card-header">{{$title}}</div>
            <div class="card-body">
                <div class="row">
                    @foreach ($taxdetail as $value)
                    <div class="col-sm-3">
                        <div class="card text-center">
                            <div class="card-header">{{$value['country']}}</div>
                            <div class="card-body">
                                {{$value['title']}}
                            </div>
                            <div class="card-footer">
                            <div class=""><a href="{{route('taxbase.country',$value->id_tax_base_detail)}}">Go To</a></div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
