@extends('layouts.auth')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="card card-primary">
            <div class="card-header">{{$taxdetail['title']}} Nomor : <span>{{$taxdetail['number']}}</span></div>
            <div class="card-body">
                <p class="text-muted mb-1">Kategori : {{$taxdetail['category']['tax_category_name']}}</p>
                <p>{{$taxdetail['sort_description']}}</p>
            </div>
            <div class="card-footer">
                <div class="row">
                    <p class="mr-5"><i class="fas fa-calendar"></i> {{date('d-M-Y',strtotime($taxdetail['created_at']))}}</p>
                    <p><i class="fas fa-share"></i> Share</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="card card-primary">
            <div class="card-header">Detail</div>
            <div class="card-body" style="overflow-y: scroll; height:280px">
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <div class="form-group nav-link p-0">
                                <!-- <a href="#" data-toggle="modal" data-target="#riwayat" class="text-muted mb-0"><i class="fas fa-link mr-2"></i> Tekait</a>
                                <div class="dropdown-divider"></div> -->
                                <a href="{{route('taxbase.countrys',$taxdetail['id_tax_base'])}}" class="text-muted mb-0"><i class="fas fa-list mr-2"></i> Daftar Treaty</a>
                                <div class="dropdown-divider"></div>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="card card-primary">
            <div class="card-header">Document</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <textarea class="form-control" id="detail_description">{{$taxdetail['detail_description']}}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="riwayat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title" id="exampleModalLabel">Peraturan Terkait</h5>
            </div>
            <div class="modal-body" style="overflow-y: scroll; height:700px">
                @foreach ($terkait as $value)
                <div class="card">
                    <div class="card-header">{{$value['title']}} Nomor : <span>{{$value['number']}}</div>
                    <div class="card-body">
                        <p class="text-muted mb-1">Kategori : {{$value['category']['tax_category_name']}}</p>
                        <p>{{$value['sort_description']}}</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <p class="mr-5"><i class="fas fa-calendar"></i> {{date('d-M-Y',strtotime($value['created_at']))}}</p>
                            <div class="mr-5"><a href="{{route('taxbase.reading',$value->id_tax_base_detail)}}"><i class="fas fa-eye"></i> View</a></div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="modal-footer">
                <a type="button" class="btn btn-sm btn-danger" href="#" data-dismiss="modal"><i class="fas fa-close"></i> Close</a>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $('#detail_description').summernote({
        airMode: true
    })
</script>
@endpush
