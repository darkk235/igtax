@extends('layouts.auth')
@section('content')
<div class="card">
    <div class="card-header">{{$title}}</div>
    <div class="card-body">
        <div class="row">
            @foreach ($taxbase as $value)
            <div class="col-sm-3">
                <div class="card text-center card-primary">
                    <div class="card-header"><strong>{{$value['tax_base_name']}}</strong></div>
                    <div class="card-body p-2">
                        <img src="https://taxbase.taxsam.co/assets/TAX_1-c20f93b39035fa677aee988fb30cd15d4229423adea455e6bc90fb4e685b1a8d.png" alt="" style="width: 100%; height:300px">
                    </div>
                    <div class="card-footer p-2">
                        <a class="pdfRender" href="{{route('taxbase.show',$value['id_tax_base'])}}"><strong>Go To</strong></a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection