@extends('layouts.auth')
@section('content')
<div class="card">
    <div class="card-header">
        <ul class="nav nav-pills">
            @foreach ($category as $key => $value)
                <li class="nav-item"><a class="nav-link {{$key == 0 ? "active":""}}" href="#codex{{$value['category_code']}}" data-toggle="tab">{{$value['category_name']}}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content">
            @foreach ($category as $keys => $values)
            <div class=" {{$keys == 0 ? "active":""}} tab-pane" id="codex{{$values['category_code']}}">
                <div class="row">
                    @foreach ($values['videos'] as $videos)
                    <div class="col-sm-2">
                        <div class="card text-center">
                            <div class="card-header"><strong>{{$videos['title']}}</strong></div>
                            <iframe src="{{$videos['url_video']}}" allowfullscreen allow="autoplay"></iframe>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
