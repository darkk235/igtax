@extends('layouts.auth')
@section('content')
<div class="card">
    <div class="card-header">
        <ul class="nav nav-pills">
            @foreach ($category as $key => $value)
            <li class="nav-item"><a class="nav-link {{$key == 0 ? "active":""}}" href="#books{{$value['category_code']}}" data-toggle="tab">{{$value['category_name']}}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content">
            @foreach ($category as $keys => $values)
            <div class=" {{$keys == 0 ? "active":""}} tab-pane" id="books{{$values['category_code']}}">
                <div class="row">
                    @foreach ($values['books'] as $books)
                    <div class="col-sm-2">
                        <div class="card text-center">
                            <div class="card-header"><strong>{{$books['title']}}</strong></div>
                            <div class="card-body p-1">
                                <img style="width: 100%; height:280px" src="{{asset('books/'.$books['cover'])}}" alt="">
                            </div>
                            <div class="card-footer p-2">
                                <a target="_blank" class="pdfRender" href="{{url('books/'.$books['file_upload'])}}"><strong>Reading</strong></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
<!-- <div id="my_pdf_viewer">
    <div id="canvas_container">
        <canvas id="pdf_renderer"></canvas>
    </div>
    <div id="navigation_controls">
        <button id="go_previous">Previous</button>
        <input id="current_page" value="1" type="number" />
        <button id="go_next">Next</button>
    </div>
    <div id="zoom_controls">
        <button id="zoom_in">+</button>
        <button id="zoom_out">-</button>
    </div>
</div> -->

@endsection

{{-- @push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"></script>
<script>
    $('.pdfRender').click(function(e){
        e.preventDefault()
        pdfRender($(this).attr('href'))
    })

    function pdfRender(files) {
        var myState = {
            pdf: null,
            currentPage: 1,
            zoom: 1
        }

        pdfjsLib.getDocument(files).then((pdf) => {
            myState.pdf = pdf;
            render();
        });

        function render() {
            myState.pdf.getPage(myState.currentPage).then((page) => {

                var canvas = document.getElementById("pdf_renderer");
                var ctx = canvas.getContext('2d');

                var viewport = page.getViewport(myState.zoom);
                canvas.width = viewport.width;
                canvas.height = viewport.height;

                page.render({
                    canvasContext: ctx,
                    viewport: viewport
                });
            });
        }
    }
</script>
@endpush --}}