@extends('layouts.guest')
@section('content')
    <div class="card-body">
        @if (!empty(session('status')))
        <p class="login-box-msg">{{session('status')}}</p>
        @endif

        <form method="POST" action="{{ route('password.store') }}">
            @csrf
        <input type="hidden" name="token" value="{{ $request->route('token') }}">

            <div class="row">
                <div class="form-group col-md-12 mb-3">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" value="{{old('email',$request->email)}}">
                    @error('email')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12 mb-3">
                    <label for="password">Password Baru</label>
                    <input type="password" name="password" class="form-control" autocomplete="new-password">
                    @error('password')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12 mb-3">
                    <label for="password_confirmation">Konfirmasi Password Baru</label>
                    <input type="password" name="password_confirmation" class="form-control" autocomplete="new-password">
                    @error('password_confirmation')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary btn-block">Reset Password</button>
                </div>
            </div>
        </form>
    </div>

@endsection
    
