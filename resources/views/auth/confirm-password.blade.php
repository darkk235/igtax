@extends('layouts.guest')
@section('content')
    <div class="card-body">
        <p class="login-box-msg">Ini adalah area aplikasi yang aman. Harap konfirmasi kata sandi Anda sebelum melanjutkan</p>

        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <div class="row">
                <div class="form-group col-md-12 mb-3">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" autocomplete="new-password">
                    @error('password')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>
                <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary btn-block">Confirm</button>
                </div>
            </div>
        </form>
    </div>

@endsection
    

