@extends('layouts.guest')
@section('content')
    <div class="card-body">
        @if (!empty(session('status')))
        <p class="login-box-msg">{{session('status')}}</p>
        @endif

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="name">Nama</label>
                    <input type="text" name="name" class="form-control" value="{{old("name")}}">
                    @error('name')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" value="{{old("email")}}">
                    @error('email')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" value="{{old("username")}}">
                    @error('username')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12 mb-3">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" autocomplete="new-password">
                    @error('password')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12 mb-3">
                    <label for="password_confirmation">Konfirmasi Password</label>
                    <input type="password" name="password_confirmation" class="form-control" autocomplete="new-password">
                    @error('password_confirmation')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary btn-block">Register</button>
                </div>
            </div>
        </form>
        <div class="d-flex justify-content-center">
            Sudah punya akun ? <a class="" href="{{ route('login') }}">&nbsp; Login</a>
        </div>
    </div>

@endsection
    