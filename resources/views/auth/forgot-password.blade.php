@extends('layouts.guest')
@section('content')
    <div class="card-body">
        @if (!empty(session('status')))
        <p class="login-box-msg">{{session('status')}}</p>
        @endif

        <p>Lupa kata sandi Anda? Tidak masalah. Cukup beri tahu kami alamat email Anda dan kami akan mengirimkan email berisi tautan pengaturan ulang kata sandi yang memungkinkan Anda memilih yang baru</p>

        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control">
                    @error('email')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary btn-block">Send</button>
                </div>
                <div class="col-12 mb-3">
                    <a href="{{route('login')}}" type="button" class="btn btn-danger btn-block">Kembali</a>
                </div>
            </div>
        </form>
    </div>
@endsection