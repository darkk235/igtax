 @extends('layouts.guest')
@section('content')
    <div class="card-body">
        @if (!empty(session('status')))
        <p class="login-box-msg">{{session('status')}}</p>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row">
                <div class="form-group col-md-12">
                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control">
                    @error('username')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="form-group col-md-12 mb-3">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control">
                    @error('password')
                    <small class="text-danger ml-1">{{$message}}</small>
                    @enderror
                </div>

                <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary btn-block">LogIn</button>
                </div>
                
                <div class="col-md-12 d-flex justify-content-center">
                    @if (Route::has('password.request'))
                        <a class="" href="{{ route('password.request') }}">Lupa password?</a>
                    @endif
                </div>
            </div>
        </form>
        <div class="d-flex justify-content-center">
            Belum punya akun ? <a class="" href="{{ route('register') }}">&nbsp; Register</a>
        </div>
    </div>
@endsection