@extends('layouts.guest')
@section('content')
    <div class="card-body">
        <p class="login-box-msg">Terima kasih telah mendaftar! Sebelum memulai, bisakah Anda memverifikasi alamat email Anda dengan mengeklik tautan yang baru saja kami kirimkan melalui email kepada Anda? Jika Anda tidak menerima email tersebut, kami dengan senang hati akan mengirimkan email lainnya..</p>

        @if (session('status') == 'verification-link-sent')
        <div class="alert alert-success">
            <p>Tautan verifikasi baru telah dikirim ke alamat email yang Anda berikan saat pendaftaran.</p>
        </div>
        @endif

        <form method="POST" action="{{ route('verification.send') }}">
            @csrf
            <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary btn-block">Kirim Ulang Verifikasi Email</button>
                </div>
            <div>
        </form>

        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <div class="col-12 mb-3">
                    <button type="submit" class="btn btn-primary btn-block">Logout</button>
                </div>
            <div>
        </form>
    </div>

@endsection
    


